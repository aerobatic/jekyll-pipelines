module Jekyll

  # Custom liquid tag for embedding a websequencediagram
  # http://www.websequencediagrams.com/embedding.html
  # Originally taken from https://github.com/robinhowlett/jekyll-wsd-tag
  # but adapted for latest version of Jekyll.
  class WsdBlock < Liquid::Block
    include Liquid::StandardFilters

    def initialize(tag_name, content, params)
      super
    end

    def render(context)
      code = h(super).strip

      <<-HTML
<div class="wsd" wsd_style="modern-blue">
  <pre>#{code}</pre>
</div><script src="https://www.websequencediagrams.com/service.js"></script>
      HTML
    end
  end
end

Liquid::Template.register_tag('wsd', Jekyll::WsdBlock)
