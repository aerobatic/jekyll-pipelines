# jekyll-pipelines

A sample template demonstrating how to setup CI/CD for a Jekyll using Bitbucket Pipelines and deploying to [Aerobatic](https://www.aerobatic.com).

Read the [companion blog post](https://www.aerobatic.com/blog/jekyll-bitbucket-pipelines/).
